import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, filter } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';


@Injectable()
export class GetPeopleService {

  constructor(private http: HttpClient) { }

  /**
   * getPeople
   */
  public getPeople(id: String): Observable<any> {

    return this.http.get('https://swapi.co/api/people/' + id);
  }

  public getImage(search: String): Observable<any> {

    const _header = new HttpHeaders({
      'Ocp-Apim-Subscription-Key': 'c1f58b221bd0476784160c3b5a20bd43'
    });

    return this.http.get('https://api.cognitive.microsoft.com/bing/v7.0/images/search?q=' + search, {headers: _header});
  }

  public getFilms(url: string): Observable<any> {
    return this.http.get(url);
  }

}
